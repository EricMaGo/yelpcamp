const express    = require("express"),
      middleware = require("../middleware"),
      router     = express.Router({mergeParams: true});

var   Campground  = require("../models/campground"),
      Comment     = require("../models/comment");

// ===================================
//          COMMENTS ROUTES
// ===================================
router.get("/new", middleware.isLoggedIn, (req, res) => {
  Campground.findById(req.params.id, (err, campground) => {
    if (err) {
      console.log(err);
    } else {
      res.render("comments/new", {
        campground: campground
      });
    }
  });
});

router.post("/", middleware.isLoggedIn, (req, res) => {
  Campground.findById(req.params.id, (err, campground) => {
    if (err) {
      console.log(err);
      res.redirect("/campgrounds");
    } else {
      Comment.create(req.body.comment, (err, comment) => {
        if (err) {
          req.flash("error", "Something went wrong");
          console.log(err);
        } else {
          comment.author.id = req.user._id;
          comment.author.username = req.user.username;
          comment.save();
          campground.comments.push(comment);
          campground.save();
          req.flash("success", "Comment successfully added!");
          res.redirect("/campgrounds/" + campground._id);
        }
      });
    }
  });
});

router.get("/:idComm/edit", middleware.checkCommentsOwnership, (req, res) => {
  Comment.findById(req.params.idComm, (err, foundComment) => {
    if (err) {
      res.redirect("back");
    } else {
      res.render("comments/edit", {
        campground_id: req.params.id,
        comment: foundComment
      });
    }
  });
});

router.put("/:idComm", middleware.checkCommentsOwnership, (req, res) => {
  Comment.findByIdAndUpdate(req.params.idComm, req.body.comment, (err, updatedComment) => {
    if (err) {
      res.redirect("back");
    } else {
      res.redirect("/campgrounds/" + req.params.id);
    }
  });
});

router.delete("/:idComm", middleware.checkCommentsOwnership, (req, res) => {
  Comment.findByIdAndRemove(req.params.idComm, (err) => {
    if (err) {
      console.log(err)
      res.redirect("back");
    } else {
      req.flash("success", "Comment deleted");
      res.redirect("/campgrounds/" + req.params.id);
    }
  });
});

module.exports = router;
