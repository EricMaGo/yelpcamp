const middleware = require("../middleware"),
      cloudinary = require("cloudinary"),
      express    = require("express"),
      multer     = require("multer"),
      router     = express.Router();

var Campground = require("../models/campground"),
    Comment    = require("../models/comment"),
    Review     = require("../models/review");

var storage = multer.diskStorage({
  filename: (req, file, callback) => {
    callback(null, Date.now() + file.originalname);
  }
});

var imageFilter = (req, file, cb) => {
  //accept image files only
  if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/i)) {
    return cb(new Error("Only image files are allowed!"), false);
  }
  cb(null, true);
};

var upload = multer({
  storage: storage,
  fileFilter: imageFilter
});

cloudinary.config({
  cloud_name: 'ericmago',
  api_key: process.env.YELPAPIKEY,
  api_secret: process.env.YELPAPISECRET
});

//  ===================================
//          CAMPGROUND ROUTES
//  ===================================
//INDEX --Show all campgrounds
router.get("/", (req, res) => {
  Campground.find({}, (err, allCampgrounds) => {
    if (err) {
      console.log(err);
    } else {
      res.render("campgrounds/index", {
        campgrounds: allCampgrounds, page: 'campgrounds'
      });
    }
  });
});

//CREATE --Add new campground to DB
router.post("/", middleware.isLoggedIn, upload.single("image"), (req, res) => {
  cloudinary.v2.uploader.upload(req.file.path, (err, result) => {
    if (err) {
      req.flash("error", err.message);
      return res.redirect("back");
    }
    var newCampground = {
      name: req.body.name,
      image: result.secure_url,
      imageId: result.public_id,
      price: req.body.price,
      description: req.body.description,
      author: {
        id: req.user._id,
        username: req.user.username
      }
    };
    Campground.create(newCampground, (err, newlyCreated) => {
      if (err) {
        req.flash("error", err.message);
        return res.redirect("back");
      } else {
        res.redirect("/campgrounds/");
      }
    });
  });
});

//NEW --Show form to create new campground
router.get("/new", middleware.isLoggedIn, (req, res) => {
  res.render("campgrounds/new");
});

router.get("/:id", function(req, res) {
  //find the campground with provided ID
  Campground.findById(req.params.id).populate("comments").populate({
    path: "reviews",
    options: {
      sort: {
        createdAt: -1
      }
    }
  }).exec(function(err, foundCampground) {
    if (err) {
      console.log(err);
    } else {
      return res.render("campgrounds/show", {
        campground: foundCampground
      });
    }
  });
});

//EDIT --Edit campground
router.get("/:id/edit", middleware.checkCampgroundOwnership, (req, res) => {
  Campground.findById(req.params.id, (err, foundCampground) => {
    if (err) {
      return res.render("back");
    } else {
      return res.render("campgrounds/edit", {
        campground: foundCampground
      });
    }
  });
});

//UPDATE --Update campground
router.put("/:id", middleware.checkCampgroundOwnership, (req, res) => {
  Campground.findByIdAndUpdate(req.params.id, req.body.campground, (err, updatedCampground) => {
    if (err) {
      req.flash("error", "Campground not found");
      return res.redirect("/campgrounds");
    } else {
      return res.redirect("/campgrounds/" + req.params.id);
    }
  });
});

//  DESTROY --Delete campground
router.delete("/:id", middleware.checkCampgroundOwnership, function(req, res) {
  Campground.findById(req.params.id, function(err, campground) {
    if (err) {
      req.flash("error", err.message);
      return res.redirect("/campgrounds");
    } else {
      Comment.remove({
        "_id": {
          $in: campground.comments
        }
      }, function(err) {
        if (err) {
          console.log(err);
          return res.redirect("/campgrounds");
        }
        Review.remove({
          "_id": {
            $in: campground.reviews
          }
        }, function(err) {
          if (err) {
            req.flash("error", err.message);
            return res.redirect("/campgrounds");
          }
          campground.remove();
          req.flash("success", "Campground deleted successfully!");
          return res.redirect("/campgrounds");
        });
      });
    }
  });
});

module.exports = router;
