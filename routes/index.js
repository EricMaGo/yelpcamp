const express  = require("express"),
      router   = express.Router();

var passport = require("passport"),
    User     = require("../models/user"),
    middleware = require("../middleware"),
    Campground = require("../models/campground");

//  ===========================
//          ROOT ROUTE
//  ===========================
router.get("/", (req, res) => {
  res.render("landing")
});

//  =============================
//          AUTH ROUTES
//  =============================
router.get("/register", (req, res) => {
  res.render("register", {page: 'register'});
});

router.post("/register", (req, res) => {
  var newUser = new User({
    username: req.body.username,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,
    avatar: req.body.avatar
  });

  if (req.body.adminCode === process.env.ADMINCODE) {
    newUser.isAdmin = true;
  }
  User.register(newUser, req.body.password, (err, user) => {
    if (err) {
      return res.render("register", {"error": err.message});
    }
    passport.authenticate("local")(req, res, () => {
      req.flash("success", "Welcome to YelpCamp, " + user.username);
      res.redirect("/campgrounds");
    });
  });
});

router.get("/login", (req, res) => {
  res.render("login", {page: 'login'});
});

router.get("/profile", middleware.isLoggedIn, (req, res) => {
  res.render("profile");
});

router.get("/users/:id", (req, res) => {
  User.findById(req.params.id, (err, foundUser) => {
    if (err) {
      req.flash("error", "Something went wrong, probably the user doesn't exists");
      return res.redirect("/campgrounds");
    }
    Campground.find().where('author.id').equals(foundUser._id).exec((err, campgrounds) => {
      if (err) {
        req.flash("err", "Something went wrong, probably the campgrounds weren't found")
        return res.redirect("/campgrounds");
      }
      res.render("./users/show", {user: foundUser, campgrounds: campgrounds});
    });
  });
});

router.post("/login", passport.authenticate("local", {
  successRedirect: "/campgrounds",
  failureRedirect: "/login",
  failureFlash: true,
}), (req, res) => {
  //empty
})

router.get("/logout", (req, res) => {
  req.logout();
  req.flash("success", "Logged you out!");
  res.redirect("/campgrounds");
});

module.exports = router;
