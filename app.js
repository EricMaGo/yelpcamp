require('dotenv').config()

const methodOverride  = require("method-override"),
      bodyParser      = require("body-parser"),
      mongoose        = require("mongoose"),
      express         = require("express"),
      app             = express(),
      passport        = require("passport"),
      flash           = require("connect-flash"),
      LocalStrategy   = require("passport-local"),
      User            = require("./models/user");

var commentRoutes     = require("./routes/comments"),
    campgroundRoutes  = require("./routes/campgrounds"),
    reviewRoutes      = require("./routes/reviews"),
    indexRoutes       = require("./routes/index");

var Campground  = require("./models/campground"),
    Comment     = require("./models/comment");

var url  = process.env.DATABASEURL || "mongodb://localhost/yelp_camp";
var port = process.env.PORT || 8080;

mongoose.connect(url);

app.use(bodyParser.urlencoded({extended: true}));
app.set("view engine", "ejs");
app.use(express.static(__dirname + '/public'));
app.use(methodOverride("_method"));
app.use(flash());

//  ====================================
//          PASSPORT CONFIGURATION
//  ====================================

app.use(require("express-session")({
  secret: process.env.SECRET,
  resave: false,
  saveUninitialized: false
}));

app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

//  =======================================
//          END PASSPORT CONFIGURATION
//  =======================================

app.use((req, res, next) => {
  res.locals.currentUser = req.user;
  res.locals.error = req.flash("error");
  res.locals.success = req.flash("success");
  next();
})

app.use("/", indexRoutes);
app.use("/campgrounds",campgroundRoutes);
app.use("/campgrounds/:id/reviews", reviewRoutes);
app.use("/campgrounds/:id/comments",commentRoutes);

app.get("*", (req, res) => {
  res.send("Wrong page... Sorry!");
});

app.listen(port, process.env.IP, () => {
  if (process.env.PORT) {
    console.log("Yelp Camp Server Started");
  } else {
    console.log("Yelp Camp Server started at localhost:" + port);
  }
});
